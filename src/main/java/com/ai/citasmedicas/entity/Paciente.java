package com.ai.citasmedicas.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PACIENTES")
@PrimaryKeyJoinColumn(name = "ID")
@NoArgsConstructor
@AllArgsConstructor
public class Paciente extends Usuario {
	private String NSS;
	
	@Column(name = "NUM_TARJETA", nullable = false)
	private String numTarjeta;
	
	private String telefono;
	
	private String direccion;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "MEDICOS_PACIENTES",
			joinColumns = @JoinColumn(name = "PACIENTE_ID"),
			inverseJoinColumns = @JoinColumn(name = "MEDICO_ID"))
	private List<Medico> medicos;
	
	@OneToMany(mappedBy = "paciente", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Cita> citas;
	
	public String getNSS() {
		return NSS;
	}

	public void setNSS(String nSS) {
		NSS = nSS;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public List<Medico> getMedicos() {
		return medicos;
	}

	public void setMedicos(List<Medico> medicos) {
		this.medicos = medicos;
	}

	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}

	public Usuario getUsuarioInstance() {
		return (Usuario) this;
	}
}
