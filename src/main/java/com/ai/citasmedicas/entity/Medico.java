package com.ai.citasmedicas.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "MEDICOS")
@PrimaryKeyJoinColumn(name = "ID")
@NoArgsConstructor
@AllArgsConstructor
public class Medico extends Usuario {
	@Column(name = "NUM_COLEGIADO", nullable = false)
	@NotEmpty
	private String numColegiado;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "MEDICOS_PACIENTES",
			joinColumns = @JoinColumn(name = "MEDICO_ID"),
			inverseJoinColumns = @JoinColumn(name = "PACIENTE_ID"))
	private List<Paciente> pacientes;

	@OneToMany(mappedBy = "medico", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Cita> citas;
	

	public String getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}

	public List<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

	public Usuario getUsuarioInstance() {
		return (Usuario) this;
	}

	public List<Cita> getCitas() {
		return citas;
	}

	public void setCitas(List<Cita> citas) {
		this.citas = citas;
	}
}
