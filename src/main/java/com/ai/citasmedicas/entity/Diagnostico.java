package com.ai.citasmedicas.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "DIAGNOSTICOS")
@NoArgsConstructor
@AllArgsConstructor
public class Diagnostico {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_diagnosticos")
    @SequenceGenerator(name = "seq_diagnosticos", sequenceName = "seq_diagnosticos", allocationSize = 1)
    private Long id;

    @Column(name = "VALORACION_ESPECIALISTA", nullable = false)
    private String valoracionEspecialista;

    private String enfermedad;

    @OneToOne()
    @JoinColumn(name="CITA_ID")
    private Cita cita;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValoracionEspecialista() {
        return valoracionEspecialista;
    }

    public void setValoracionEspecialista(String valoracionEspecialista) {
        this.valoracionEspecialista = valoracionEspecialista;
    }

    public String getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(String enfermedad) {
		this.enfermedad = enfermedad;
	}

	public Cita getCita() {
		return cita;
	}

	public void setCita(Cita cita) {
		this.cita = cita;
	}
}
