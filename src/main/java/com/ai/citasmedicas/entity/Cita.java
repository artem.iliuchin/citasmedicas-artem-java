package com.ai.citasmedicas.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CITAS")
@NoArgsConstructor
@AllArgsConstructor
public class Cita {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_citas")
	@SequenceGenerator(name = "seq_citas", sequenceName = "seq_citas", allocationSize = 1)
    private Long id;

    @Column(name = "FECHA_HORA", nullable = false)
    private Date fechaHora;

    @Column(name = "MOTIVO_CITA", nullable = false)
    private String motivoCita;

    @ManyToOne
    @JoinColumn(name = "MEDICO_ID")
    private Medico medico;

    @ManyToOne
    @JoinColumn(name = "PACIENTE_ID")
    private Paciente paciente;

	@OneToMany(mappedBy = "cita", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Diagnostico> diagnostico;

    public Date getFechaHora() {
        return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getMotivoCita() {
		return motivoCita;
	}

	public void setMotivoCita(String motivoCita) {
		this.motivoCita = motivoCita;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long iD) {
		id = iD;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public List<Diagnostico> getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(List<Diagnostico> diagnostico) {
		this.diagnostico = diagnostico;
	}
}
