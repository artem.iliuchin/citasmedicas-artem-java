package com.ai.citasmedicas.repository;

import com.ai.citasmedicas.entity.Cita;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CitaRepository extends JpaRepository<Cita, Long> {

}
