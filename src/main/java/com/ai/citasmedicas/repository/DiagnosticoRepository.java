package com.ai.citasmedicas.repository;

import com.ai.citasmedicas.entity.Diagnostico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiagnosticoRepository extends JpaRepository<Diagnostico, Long> {

}
