package com.ai.citasmedicas.entityDTO;

import java.util.List;

public class MedicoDTO extends UsuarioDTO {
	private String numColegiado;

	private List<PacienteSDTO> pacientes;

	private List<CitaDTO> citas;

	public MedicoDTO() {}

	public String getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}

	public List<PacienteSDTO> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<PacienteSDTO> pacientes) {
		this.pacientes = pacientes;
	}

	public List<CitaDTO> getCitas() {
		return citas;
	}

	public void setCitas(List<CitaDTO> citas) {
		this.citas = citas;
	}
}
