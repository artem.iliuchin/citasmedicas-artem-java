package com.ai.citasmedicas.entityDTO;

import java.util.Date;
import java.util.List;

public class CitaDTO {
	private Long id;

	private Date fechaHora;

	private String motivoCita;

	private MedicoSDTO medico;

	private PacienteSDTO paciente;

	private List<DiagnosticoSDTO>  diagnosticos;

	public CitaDTO() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getMotivoCita() {
		return motivoCita;
	}

	public void setMotivoCita(String motivoCita) {
		this.motivoCita = motivoCita;
	}

	public MedicoSDTO getMedico() {
		return medico;
	}

	public void setMedico(MedicoSDTO medico) {
		this.medico = medico;
	}

	public PacienteSDTO getPaciente() {
		return paciente;
	}

	public void setPaciente(PacienteSDTO paciente) {
		this.paciente = paciente;
	}

	public List<DiagnosticoSDTO> getDiagnosticos() {
		return diagnosticos;
	}

	public void setDiagnosticos(List<DiagnosticoSDTO> diagnosticos) {
		this.diagnosticos = diagnosticos;
	}
}
