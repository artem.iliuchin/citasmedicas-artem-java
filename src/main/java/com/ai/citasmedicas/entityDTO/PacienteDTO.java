package com.ai.citasmedicas.entityDTO;

import java.util.List;

public class PacienteDTO extends UsuarioDTO {

    private String NSS;

    private String numTarjeta;

    private String telefono;

    private String direccion;

    private List<MedicoSDTO> medicos;

    private List<CitaDTO> citas;

    public PacienteDTO() {}

    public String getNSS() {
        return NSS;
    }

    public void setNSS(String NSS) {
        this.NSS = NSS;
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }

    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<MedicoSDTO> getMedicos() {
        return medicos;
    }

    public void setMedicos(List<MedicoSDTO> medicos) {
        this.medicos = medicos;
    }

    public List<CitaDTO> getCitas() {
        return citas;
    }

    public void setCitas(List<CitaDTO> citas) {
        this.citas = citas;
    }

}
