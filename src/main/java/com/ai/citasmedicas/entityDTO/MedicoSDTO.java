package com.ai.citasmedicas.entityDTO;

public class MedicoSDTO extends UsuarioDTO {

    private String numColegiado;

    public MedicoSDTO() {}

    public String getNumColegiado() {
        return numColegiado;
    }

    public void setNumColegiado(String numColegiado) {
        this.numColegiado = numColegiado;
    }

}
