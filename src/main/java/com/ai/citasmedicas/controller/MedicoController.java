package com.ai.citasmedicas.controller;

import com.ai.citasmedicas.entityDTO.DiagnosticoDTO;
import com.ai.citasmedicas.entityDTO.MedicoDTO;
import com.ai.citasmedicas.service.MedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/medicos")
public class MedicoController {
	@Autowired
	private MedicoService medicoService;


	@GetMapping
	public ResponseEntity<List<MedicoDTO>> getAll() {
		return ResponseEntity.ok(medicoService.getAll());
	}

	@GetMapping("/{id}")
	public MedicoDTO getByID(@PathVariable Long id) {
		return medicoService.getById(id);
	}

	@GetMapping("/by-username/{username}")
	public MedicoDTO getByUsername(@PathVariable String username) {
		return medicoService.findByUsuario(username);
	}

	@PutMapping
	public MedicoDTO update(@Validated @RequestBody MedicoDTO medicoDTO){
		medicoService.update(medicoDTO);

		return medicoDTO;
	}
	@PostMapping
	public MedicoDTO create(@Validated @RequestBody MedicoDTO medicoDTO) {
		medicoService.create(medicoDTO);

		return medicoDTO;
	}

	@DeleteMapping("/{id}")
	public Long delete(@PathVariable Long id) {
		medicoService.delete(id);

		return id;
	}

}
