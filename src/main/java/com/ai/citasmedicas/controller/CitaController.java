package com.ai.citasmedicas.controller;

import com.ai.citasmedicas.entityDTO.CitaDTO;
import com.ai.citasmedicas.service.CitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/citas")
public class CitaController {
    @Autowired
    private CitaService citaService;

    @GetMapping
    public List<CitaDTO> getAll() {
        return citaService.getAll();
    }

    @GetMapping("/{id}")
    public CitaDTO getByID(@PathVariable Long id) {
        return citaService.getById(id);
    }

    @PutMapping
    public CitaDTO update(@Validated @RequestBody CitaDTO citaDTO) {
        citaService.update(citaDTO);

        return citaDTO;
    }

    @PostMapping
    public CitaDTO create(@Validated @RequestBody CitaDTO citaDTO) {
        citaService.create(citaDTO);

        return citaDTO;
    }

    @DeleteMapping("/{id}")
    public Long delete(@PathVariable Long id) {
        citaService.delete(id);

        return id;
    }

}
