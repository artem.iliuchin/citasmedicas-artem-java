package com.ai.citasmedicas.controller;


import com.ai.citasmedicas.security.JwtUtil;
import com.ai.citasmedicas.service.UsuarioDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UsuarioController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UsuarioDetailService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/login")
    public ResponseEntity<Map<String, String>> createAuthenticationToken(@RequestBody Map<String, String> credentials)throws Exception {
        String user = credentials.get("username");
        String password = credentials.get("password");
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user, password));
        } catch (BadCredentialsException e) {
            throw new Exception("Credenciales incorrectas", e);
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(user);
        final String jwt = "Bearer " + jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok(Collections.singletonMap("token", jwt));
    }
}
