package com.ai.citasmedicas.controller;

import com.ai.citasmedicas.entityDTO.CitaDTO;
import com.ai.citasmedicas.entityDTO.DiagnosticoDTO;
import com.ai.citasmedicas.service.DiagnosticoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/diagnosticos")
public class DiagnosticoController {
	@Autowired
	private DiagnosticoService diagnosticoService;

	@GetMapping
	public List<DiagnosticoDTO> getAll() {
		return diagnosticoService.getAll();
	}

	@GetMapping("/{id}")
	public DiagnosticoDTO getByID(@PathVariable Long id) {
		return diagnosticoService.getByID(id);
	}

	@PutMapping
	public  DiagnosticoDTO update(@Validated @RequestBody DiagnosticoDTO diagnosticoDTO){
		diagnosticoService.update(diagnosticoDTO);
		return diagnosticoDTO;
	}

	@PostMapping
	public DiagnosticoDTO create(@Validated @RequestBody DiagnosticoDTO diagnosticoDTO) {
		diagnosticoService.create(diagnosticoDTO);

		return diagnosticoDTO;
	}

	@DeleteMapping("/{id}")
	public Long delete(@PathVariable Long id) {
		diagnosticoService.delete(id);

		return id;
	}

}
