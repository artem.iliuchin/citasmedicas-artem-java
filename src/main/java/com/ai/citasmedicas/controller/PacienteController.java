package com.ai.citasmedicas.controller;

import com.ai.citasmedicas.entityDTO.MedicoDTO;
import com.ai.citasmedicas.entityDTO.PacienteDTO;
import com.ai.citasmedicas.service.PacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/pacientes")
public class PacienteController {
	@Autowired
	private PacienteService pacienteService;

	@GetMapping
	public List<PacienteDTO> getAll() {
		return pacienteService.getAll();
	}

	@GetMapping("/{id}")
	public PacienteDTO getByID(@PathVariable Long id) {
		return pacienteService.getById(id);
	}

	@GetMapping("/by-username/{username}")
	public PacienteDTO getByUsername(@PathVariable String username) {
		return pacienteService.findByUsuario(username);
	}

	@PutMapping
	public PacienteDTO update(@Validated @RequestBody PacienteDTO pacienteDTO){
		pacienteService.update(pacienteDTO);

		return pacienteDTO;
	}
	@PostMapping
	public PacienteDTO create(@Validated @RequestBody PacienteDTO pacienteDTO) {
		pacienteService.create(pacienteDTO);

		return pacienteDTO;
	}

	@DeleteMapping("/{id}")
	public Long delete(@PathVariable Long id) {
		pacienteService.delete(id);

		return id;
	}

}
