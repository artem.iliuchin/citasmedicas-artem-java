package com.ai.citasmedicas.mapper;

import com.ai.citasmedicas.entity.Cita;
import com.ai.citasmedicas.entity.Diagnostico;
import com.ai.citasmedicas.entityDTO.CitaDTO;
import com.ai.citasmedicas.entityDTO.DiagnosticoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DiagnosticoMapper {
	DiagnosticoMapper INSTANCE = Mappers.getMapper(DiagnosticoMapper.class);


	DiagnosticoDTO diagnosticoToDiagnosticoDTO(Diagnostico diagnostico);

	Diagnostico diagnosticoDTOToDiagnostico(DiagnosticoDTO DiagnosticoDTO);

}