package com.ai.citasmedicas.mapper;

import com.ai.citasmedicas.entity.Cita;
import com.ai.citasmedicas.entityDTO.CitaDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CitaMapper {
	CitaMapper INSTANCE = Mappers.getMapper(CitaMapper.class);

	CitaDTO citaToCitaDTO(Cita cita);

	Cita citaDTOToCita(CitaDTO citaDTO);
}