package com.ai.citasmedicas.mapper;

import com.ai.citasmedicas.entity.Usuario;
import com.ai.citasmedicas.entityDTO.UsuarioDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UsuarioMapper {
	UsuarioMapper INSTANCE = Mappers.getMapper(UsuarioMapper.class);
	
	UsuarioDTO usuarioToUsuarioDTO(Usuario usuario);

	Usuario usuarioDTOToUsuario(UsuarioDTO usuario);
}
