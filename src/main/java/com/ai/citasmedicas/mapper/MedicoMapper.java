package com.ai.citasmedicas.mapper;

import com.ai.citasmedicas.entity.Cita;
import com.ai.citasmedicas.entity.Medico;
import com.ai.citasmedicas.entityDTO.CitaDTO;
import com.ai.citasmedicas.entityDTO.MedicoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface MedicoMapper {
	MedicoMapper INSTANCE = Mappers.getMapper(MedicoMapper.class);


	MedicoDTO medicoToMedicoDTO(Medico medico);

	Medico medicoDTOToMedico(MedicoDTO MedicoDTO);
}
