package com.ai.citasmedicas.mapper;

import com.ai.citasmedicas.entity.Medico;
import com.ai.citasmedicas.entity.Paciente;
import com.ai.citasmedicas.entityDTO.MedicoDTO;
import com.ai.citasmedicas.entityDTO.PacienteDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PacienteMapper {
	PacienteMapper INSTANCE = Mappers.getMapper(PacienteMapper.class);
	
	PacienteDTO pacienteToPacienteDTO(Paciente paciente);

	Paciente pacienteDTOToPaciente(PacienteDTO PacienteDTO);
	
}
