package com.ai.citasmedicas.service;

import com.ai.citasmedicas.entity.Cita;
import com.ai.citasmedicas.entity.Diagnostico;
import com.ai.citasmedicas.entity.Medico;
import com.ai.citasmedicas.entity.Paciente;
import com.ai.citasmedicas.entityDTO.CitaDTO;
import com.ai.citasmedicas.entityDTO.DiagnosticoDTO;
import com.ai.citasmedicas.entityDTO.DiagnosticoSDTO;
import com.ai.citasmedicas.exception.ResourceNotFoundException;
import com.ai.citasmedicas.mapper.CitaMapper;
import com.ai.citasmedicas.repository.CitaRepository;
import com.ai.citasmedicas.repository.DiagnosticoRepository;
import com.ai.citasmedicas.repository.MedicoRepository;
import com.ai.citasmedicas.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CitaService {
	@Autowired
	CitaRepository citaRepository;

	@Autowired
	private DiagnosticoRepository diagnosticoRepository;

	@Autowired
	private MedicoRepository medicoRepository;

	@Autowired
	private PacienteRepository pacienteRepository;

	public List<CitaDTO> getAll() {
		List<CitaDTO> citasDTO = new ArrayList<>();
		for (Cita cita : citaRepository.findAll()) {
			citasDTO.add(CitaMapper.INSTANCE.citaToCitaDTO(cita));
		}

		return citasDTO;
	}

	public CitaDTO getById(Long id) {
		Optional<Cita> cita = citaRepository.findById(id);

		return CitaMapper.INSTANCE.citaToCitaDTO(cita.get());
	}

	public CitaDTO create(CitaDTO citaDTO) {
		Cita cita = CitaMapper.INSTANCE.citaDTOToCita(citaDTO);
		cita = citaRepository.save(cita);
		return CitaMapper.INSTANCE.citaToCitaDTO(cita);
	}

	public CitaDTO update(CitaDTO citaDTO) {

		Cita cita = citaRepository.findById(citaDTO.getId())
				.orElseThrow(() -> new ResourceNotFoundException("No encontrada la cita"));

		Optional<Medico> medico = medicoRepository.findById(citaDTO.getMedico().getId());
		Optional<Paciente> paciente = pacienteRepository.findById(citaDTO.getPaciente().getId());

		if(citaDTO.getDiagnosticos() != null){
			List<Diagnostico> diagnosticos = new ArrayList<>();
			for(DiagnosticoSDTO diagnosticoDTO:citaDTO.getDiagnosticos()){
				Optional<Diagnostico> diagnostico = diagnosticoRepository.findById(diagnosticoDTO.getId());
				diagnosticos.add(diagnostico.get());
			}
			cita.setDiagnostico(diagnosticos);
		}


		cita.setMotivoCita(citaDTO.getMotivoCita());
		cita.setFechaHora(citaDTO.getFechaHora());
		cita.setMotivoCita(citaDTO.getMotivoCita());
		cita.setPaciente(paciente.get());
		cita.setMedico(medico.get());
		cita = citaRepository.save(cita);

		return CitaMapper.INSTANCE.citaToCitaDTO(cita);
	}

	public void delete(Long id) {
		if (!citaRepository.existsById(id)) {
			throw new ResourceNotFoundException("Cita not found : " + id);
		}

		citaRepository.deleteById(id);
	}

}
