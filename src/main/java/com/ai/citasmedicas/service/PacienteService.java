package com.ai.citasmedicas.service;


import com.ai.citasmedicas.entity.Cita;
import com.ai.citasmedicas.entity.Medico;
import com.ai.citasmedicas.entity.Paciente;
import com.ai.citasmedicas.entityDTO.*;
import com.ai.citasmedicas.exception.ResourceNotFoundException;
import com.ai.citasmedicas.mapper.MedicoMapper;
import com.ai.citasmedicas.mapper.PacienteMapper;
import com.ai.citasmedicas.repository.CitaRepository;
import com.ai.citasmedicas.repository.MedicoRepository;
import com.ai.citasmedicas.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PacienteService {
	@Autowired
	private PacienteRepository pacienteRepository;

	@Autowired
	private MedicoRepository medicoRepository;

	@Autowired
	private CitaRepository citaRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public List<PacienteDTO> getAll() {
		List<PacienteDTO> pacientesDTO = new ArrayList<PacienteDTO>();
		for (Paciente paciente : pacienteRepository.findAll()) {
			pacientesDTO.add(PacienteMapper.INSTANCE.pacienteToPacienteDTO(paciente));
		}

		return pacientesDTO;
	}

	public PacienteDTO getById(Long id) {
		Optional<Paciente> paciente = pacienteRepository.findById(id);

		return PacienteMapper.INSTANCE.pacienteToPacienteDTO(paciente.get());
	}

	public PacienteDTO findByUsuario(String usuario) {
		Optional<Paciente> paciente = pacienteRepository.findByUsuario(usuario);

		return PacienteMapper.INSTANCE.pacienteToPacienteDTO(paciente.get());
	}

	public PacienteDTO update(PacienteDTO pacienteDTO){

		Paciente paciente = pacienteRepository.findById(pacienteDTO.getId())
				.orElseThrow(()-> new ResourceNotFoundException("No encontrada paciente"));

		if(pacienteDTO.getMedicos() != null){
			List<Medico> medicos = new ArrayList<>();
			for(MedicoSDTO medicoSDTO:pacienteDTO.getMedicos()){
				Optional<Medico> medico = medicoRepository.findById(medicoSDTO.getId());
				medicos.add(medico.get());
			}
			paciente.setMedicos(medicos);
		}

		if(pacienteDTO.getCitas() != null){
			List<Cita> citas = new ArrayList<>();
			for(CitaDTO citaDTO:pacienteDTO.getCitas()){
				Optional<Cita> cita = citaRepository.findById(citaDTO.getId());
				citas.add(cita.get());
			}
			paciente.setCitas(citas);
		}

		paciente.setDireccion(pacienteDTO.getDireccion());
		paciente.setNSS(pacienteDTO.getNSS());
		paciente.setNumTarjeta(pacienteDTO.getNumTarjeta());
		paciente.setTelefono(pacienteDTO.getTelefono());
		paciente.setApellidos(pacienteDTO.getApellidos());
		paciente.setNombre(pacienteDTO.getNombre());
		paciente.setClave(pacienteDTO.getClave());
		paciente.setUsuario(pacienteDTO.getUsuario());

		paciente = pacienteRepository.save(paciente);
		return PacienteMapper.INSTANCE.pacienteToPacienteDTO(paciente);

	}

	public PacienteDTO create(PacienteDTO pacienteDTO) {
		pacienteDTO.setClave(passwordEncoder.encode(pacienteDTO.getClave()));
		Paciente paciente = PacienteMapper.INSTANCE.pacienteDTOToPaciente(pacienteDTO);
		paciente= pacienteRepository.save(paciente);
		return PacienteMapper.INSTANCE.pacienteToPacienteDTO(paciente);
	}

	public void delete(Long id) {
		if (!pacienteRepository.existsById(id)) {
			throw new ResourceNotFoundException("Paciente not found : " + id);
		}

		pacienteRepository.deleteById(id);
	}

}
