package com.ai.citasmedicas.service;

import com.ai.citasmedicas.entity.Medico;
import com.ai.citasmedicas.entity.Paciente;
import com.ai.citasmedicas.entity.Usuario;
import com.ai.citasmedicas.repository.MedicoRepository;
import com.ai.citasmedicas.repository.PacienteRepository;
import com.ai.citasmedicas.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioDetailService implements UserDetailsService
{
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private MedicoRepository medicoRepository;

    @Autowired
    private PacienteRepository pacienteRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        Optional<Usuario> usuarioRes = usuarioRepository.findByUsuario(username);

        if (usuarioRes.isEmpty())
            throw new UsernameNotFoundException("Usuario not found : " + username);

        Usuario usuario = usuarioRes.get();

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();

        if (usuario instanceof Medico) {
            authorities.add(new SimpleGrantedAuthority("MEDICO"));
        } else if (usuario instanceof Paciente) {
            authorities.add(new SimpleGrantedAuthority("PACIENTE"));
        } else {
            authorities.add(new SimpleGrantedAuthority("USER"));
        }

        return new User(
                username,
                usuario.getClave(),
                authorities
        );
    }
}
