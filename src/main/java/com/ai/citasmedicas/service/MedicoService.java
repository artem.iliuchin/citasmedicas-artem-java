package com.ai.citasmedicas.service;


import com.ai.citasmedicas.entity.Cita;
import com.ai.citasmedicas.entity.Medico;
import com.ai.citasmedicas.entity.Paciente;
import com.ai.citasmedicas.entityDTO.CitaDTO;
import com.ai.citasmedicas.entityDTO.MedicoDTO;
import com.ai.citasmedicas.entityDTO.PacienteDTO;
import com.ai.citasmedicas.entityDTO.PacienteSDTO;
import com.ai.citasmedicas.exception.ResourceNotFoundException;
import com.ai.citasmedicas.mapper.MedicoMapper;
import com.ai.citasmedicas.repository.CitaRepository;
import com.ai.citasmedicas.repository.MedicoRepository;
import com.ai.citasmedicas.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MedicoService {
    @Autowired
    private MedicoRepository medicoRepository;

    @Autowired
    private CitaRepository citaRepository;

    @Autowired
    private PacienteRepository pacienteRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<MedicoDTO> getAll() {
        List<MedicoDTO> medicosDTO = new ArrayList<>();
        for (Medico medico : medicoRepository.findAll()) {
            medicosDTO.add(MedicoMapper.INSTANCE.medicoToMedicoDTO(medico));
        }

        return medicosDTO;
    }
    public MedicoDTO getById(Long id) {
        Optional<Medico> medico = medicoRepository.findById(id);
        return MedicoMapper.INSTANCE.medicoToMedicoDTO(medico.get());

    }

    public MedicoDTO findByUsuario(String usuario) {
        Optional<Medico> medico = medicoRepository.findByUsuario(usuario);

        return MedicoMapper.INSTANCE.medicoToMedicoDTO(medico.get());
    }

    public MedicoDTO update(MedicoDTO medicoDTO){

        Medico medico = medicoRepository.findById(medicoDTO.getId())
                .orElseThrow(()-> new ResourceNotFoundException("No encontrada medico"));

        if(medicoDTO.getPacientes() != null){
            List<Paciente> pacientes = new ArrayList<>();
            for(PacienteSDTO pacienteSDTO:medicoDTO.getPacientes()){
                Optional<Paciente> paciente = pacienteRepository.findById(pacienteSDTO.getId());
                pacientes.add(paciente.get());
            }
            medico.setPacientes(pacientes);
        }

        if(medicoDTO.getCitas() != null){
            List<Cita> citas = new ArrayList<>();
            for(CitaDTO citaDTO:medicoDTO.getCitas()){
                Optional<Cita> cita = citaRepository.findById(citaDTO.getId());
                citas.add(cita.get());
            }
            medico.setCitas(citas);
        }


        medico.setNumColegiado(medicoDTO.getNumColegiado());
        medico.setApellidos(medicoDTO.getApellidos());
        medico.setNombre(medicoDTO.getNombre());
        medico.setClave(medicoDTO.getClave());
        medico.setUsuario(medicoDTO.getUsuario());

        medico = medicoRepository.save(medico);
        return MedicoMapper.INSTANCE.medicoToMedicoDTO(medico);

    }

    public MedicoDTO create(MedicoDTO medicoDTO) {
        medicoDTO.setClave(passwordEncoder.encode(medicoDTO.getClave()));
        Medico medico = MedicoMapper.INSTANCE.medicoDTOToMedico(medicoDTO);
        System.out.println(medico.getId());
        medico = medicoRepository.save(medico);
        return MedicoMapper.INSTANCE.medicoToMedicoDTO(medico);
    }

    public void delete(Long id) {
        if (!medicoRepository.existsById(id)) {
            throw new ResourceNotFoundException("Paciente not found : " + id);
        }

        medicoRepository.deleteById(id);
    }

}
