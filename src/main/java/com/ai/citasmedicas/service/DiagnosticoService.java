package com.ai.citasmedicas.service;


import com.ai.citasmedicas.entity.Cita;
import com.ai.citasmedicas.entity.Diagnostico;
import com.ai.citasmedicas.entityDTO.DiagnosticoDTO;
import com.ai.citasmedicas.exception.ResourceNotFoundException;
import com.ai.citasmedicas.mapper.DiagnosticoMapper;
import com.ai.citasmedicas.repository.CitaRepository;
import com.ai.citasmedicas.repository.DiagnosticoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DiagnosticoService {
	@Autowired
	DiagnosticoRepository diagnosticoRepository;

	@Autowired
	CitaRepository citaRepository;
	

	public List<DiagnosticoDTO> getAll() {
		List<DiagnosticoDTO> diagnosticosDTO = new ArrayList<DiagnosticoDTO>();
		for (Diagnostico diagnostico : diagnosticoRepository.findAll()) {
			diagnosticosDTO.add(DiagnosticoMapper.INSTANCE.diagnosticoToDiagnosticoDTO(diagnostico));
		}

		return diagnosticosDTO;
	}

	public DiagnosticoDTO getByID(Long id) {
		Optional<Diagnostico> diagnostico = diagnosticoRepository.findById(id);

		return DiagnosticoMapper.INSTANCE.diagnosticoToDiagnosticoDTO(diagnostico.get());
	}

	public DiagnosticoDTO update(DiagnosticoDTO diagnosticoDTO){
		Diagnostico diagnostico = diagnosticoRepository.findById(diagnosticoDTO.getId())
				.orElseThrow(() -> new ResourceNotFoundException("No encontrada diagnostico"));

		Optional<Cita> cita =citaRepository.findById(diagnosticoDTO.getId());
		diagnostico.setCita(cita.get());
		diagnostico.setEnfermedad(diagnosticoDTO.getEnfermedad());
		diagnostico.setValoracionEspecialista(diagnosticoDTO.getValoracionEspecialista());

		diagnostico= diagnosticoRepository.save(diagnostico);

		return DiagnosticoMapper.INSTANCE.diagnosticoToDiagnosticoDTO(diagnostico);

	}

	public DiagnosticoDTO create(DiagnosticoDTO diagnosticoDTO) {
		Diagnostico diagnostico = DiagnosticoMapper.INSTANCE.diagnosticoDTOToDiagnostico(diagnosticoDTO);
		diagnostico = diagnosticoRepository.save(diagnostico);
		return DiagnosticoMapper.INSTANCE.diagnosticoToDiagnosticoDTO(diagnostico);
	}

	public void delete(Long id) {
		if (!diagnosticoRepository.existsById(id)) {
			throw new ResourceNotFoundException("Diagnostico not found : " + id);
		}

		diagnosticoRepository.deleteById(id);
	}

}
