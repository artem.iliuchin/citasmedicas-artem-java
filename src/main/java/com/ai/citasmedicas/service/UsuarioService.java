package com.ai.citasmedicas.service;

import com.ai.citasmedicas.entity.Usuario;
import com.ai.citasmedicas.entityDTO.UsuarioDTO;
import com.ai.citasmedicas.mapper.UsuarioMapper;
import com.ai.citasmedicas.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    public Optional<UsuarioDTO> findByUsuario(String usuario) {
        Optional<Usuario> usuarioEntity = usuarioRepository.findByUsuario(usuario);
        if (usuarioEntity.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(UsuarioMapper.INSTANCE.usuarioToUsuarioDTO(usuarioEntity.get()));
    }
}
